package com.lineate.internal.nltc.rest.controller;

import static com.lineate.internal.nltc.common.service.Classifier.DEFAULT_PROBABILITY_THRESHOLD;

import com.lineate.internal.nltc.common.model.Category;
import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.model.Keyphrase;
import com.lineate.internal.nltc.common.service.Classifier;
import com.lineate.internal.nltc.common.service.KeyphraseExtractor;
import com.lineate.internal.nltc.common.util.TextUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ru.sladethe.common.text.StringUtil;

import javax.annotation.Nonnull;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@RequestMapping("/api")
@RestController
public class ClassifyController {
    private final Classifier classifier;
    private final KeyphraseExtractor keyphraseExtractor;

    @Autowired
    public ClassifyController(@Nonnull Classifier classifier, @Nonnull KeyphraseExtractor keyphraseExtractor) {
        this.classifier = classifier;
        this.keyphraseExtractor = keyphraseExtractor;
    }

    @PostMapping(value = "/classify")
    public List<Category> classify(@RequestBody final String text) throws IOException {
        String prettifiedText = TextUtil.prettify(text, true); // FIXME may eat linebreaks, need to rework
        List<Keyphrase> keyphrases = keyphraseExtractor.extract(new Document(prettifiedText, null));
        return classifier.classify(keyphrases, DEFAULT_PROBABILITY_THRESHOLD);
    }
}
