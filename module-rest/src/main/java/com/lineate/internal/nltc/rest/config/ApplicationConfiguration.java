package com.lineate.internal.nltc.rest.config;

import com.lineate.internal.nltc.common.service.*;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.*;
import ru.sladethe.common.io.FileUtil;

import java.io.File;
import java.io.IOException;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@Configuration
public class ApplicationConfiguration {
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
    @Bean
    public Classifier getClassifier() throws IOException {
        ClassifierImpl classifier = new ClassifierImpl();
        classifier.load(FileUtil.getCriticalFileBytes(new File("nltc.db"))); // TODO read file name from settings
        return classifier;
    }

    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
    @Bean
    public KeyphraseExtractor getKeyphraseExtractor() {
        return new RemoteKeyphraseExtractor("http://localhost:8888/api/keywords"); // TODO read URL from settings
    }
}
