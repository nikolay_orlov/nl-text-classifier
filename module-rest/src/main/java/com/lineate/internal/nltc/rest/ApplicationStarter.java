package com.lineate.internal.nltc.rest;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@SpringBootApplication(scanBasePackages = "com.lineate.internal.nltc.rest")
public class ApplicationStarter extends SpringBootServletInitializer {
    public static void main(String[] args) {
        try {
            SpringApplication.run(ApplicationStarter.class, args);
        } catch (Throwable t) {
            LoggerFactory.getLogger(ApplicationStarter.class).error("Failed to start.");
        }
    }
}
