package com.lineate.internal.nltc.common.math.vector;

import com.lineate.internal.nltc.common.pair.ImmutableIntDoublePair;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@SuppressWarnings("MessageMissingOnJUnitAssertion")
public class HppcSparseDoubleVectorTest {
    private static final double EPSILON = 1.0E-9D;

    @Test
    public void getNonZeros() {
        SparseDoubleVector vector = new HppcSparseDoubleVector(100);

        vector.set(99, -9.0D);
        vector.set(10, 1.0D);
        vector.set(20, 2.0D);
        vector.set(30, 3.0D);

        List<ImmutableIntDoublePair> nonZeros = vector.getNonZeros();

        Assert.assertEquals(new ImmutableIntDoublePair(10, 1.0D), nonZeros.get(0));
        Assert.assertEquals(new ImmutableIntDoublePair(20, 2.0D), nonZeros.get(1));
        Assert.assertEquals(new ImmutableIntDoublePair(30, 3.0D), nonZeros.get(2));
        Assert.assertEquals(new ImmutableIntDoublePair(99, -9.0D), nonZeros.get(3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void dotProductFails() {
        SparseDoubleVector vector = new HppcSparseDoubleVector(1000);
        SparseDoubleVector otherVector = new HppcSparseDoubleVector(100);

        vector.dotProduct(otherVector);
    }

    @SuppressWarnings("UnnecessaryCodeBlock")
    @Test
    public void dotProduct() {
        {
            SparseDoubleVector vector = new HppcSparseDoubleVector(100);
            SparseDoubleVector otherVector = new HppcSparseDoubleVector(100);

            vector.set(0, 1.0D);
            vector.set(1, 1000.0D);

            otherVector.set(0, 10.0D);
            otherVector.set(2, -200.0D);

            Assert.assertEquals(10.0D, vector.dotProduct(otherVector), EPSILON);
            Assert.assertEquals(10.0D, otherVector.dotProduct(vector), EPSILON);
        }

        {
            SparseDoubleVector vector = new HppcSparseDoubleVector(10000);
            SparseDoubleVector otherVector = new HppcSparseDoubleVector(10000);

            vector.set(100, -100.0D);
            vector.set(500, 999.0D);
            vector.set(1000, 1000.0D);
            vector.set(2001, 1000.0D);
            vector.set(3000, 1000000.0D);

            otherVector.set(10, 10.0D);
            otherVector.set(20, 20.0D);
            otherVector.set(30, 30.0D);
            otherVector.set(40, 40.0D);
            otherVector.set(50, 50.0D);
            otherVector.set(100, 1.0D);
            otherVector.set(1000, 1.0D);
            otherVector.set(2000, 1.0D);
            otherVector.set(3000, 0.0D);
            otherVector.set(4000, 1.0D);
            otherVector.set(5000, 1.0D);

            Assert.assertEquals(900.0D, vector.dotProduct(otherVector), EPSILON);
            Assert.assertEquals(900.0D, otherVector.dotProduct(vector), EPSILON);
        }
    }
}
