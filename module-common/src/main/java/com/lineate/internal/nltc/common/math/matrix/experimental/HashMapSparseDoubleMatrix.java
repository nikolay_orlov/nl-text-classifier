package com.lineate.internal.nltc.common.math.matrix.experimental;

import java.util.HashMap;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class HashMapSparseDoubleMatrix extends JavaMapSparseDoubleMatrix {
    public HashMapSparseDoubleMatrix(int rowCount, int colCount, double defaultValue) {
        super(rowCount, colCount, defaultValue, new HashMap<>(), HashMap::new);
    }
}
