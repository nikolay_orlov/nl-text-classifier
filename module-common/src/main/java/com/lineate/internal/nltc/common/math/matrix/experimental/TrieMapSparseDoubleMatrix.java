package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.romix.scala.collection.concurrent.TrieMap;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class TrieMapSparseDoubleMatrix extends JavaMapSparseDoubleMatrix {
    public TrieMapSparseDoubleMatrix(int rowCount, int colCount, double defaultValue) {
        super(rowCount, colCount, defaultValue, new TrieMap<>(), TrieMap::new);
    }
}
