package com.lineate.internal.nltc.common.service;

import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.util.TextUtil;
import ru.sladethe.common.lang.ObjectUtil;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Objects;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class PrettifyDocumentReader implements DocumentReader {
    @Nonnull
    private final DocumentReader reader;

    private final boolean onlyWhitelistedTokens;

    @SuppressWarnings("WeakerAccess")
    public PrettifyDocumentReader(@Nonnull DocumentReader reader, boolean onlyWhitelistedTokens) {
        this.reader = Objects.requireNonNull(reader);
        this.onlyWhitelistedTokens = onlyWhitelistedTokens;
    }

    public PrettifyDocumentReader(@Nonnull DocumentReader reader) {
        this(reader, true);
    }

    @Nullable
    @Override
    public Document read() throws IOException {
        return ObjectUtil.mapNotNull(reader.read(), document -> new Document(
                TextUtil.prettify(document.getText(), onlyWhitelistedTokens),
                document.getCategory()
        ));
    }


    @Override
    public void close() throws IOException {
        reader.close();
    }
}
