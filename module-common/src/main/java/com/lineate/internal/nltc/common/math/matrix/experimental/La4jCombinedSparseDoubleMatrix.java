package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.lineate.internal.nltc.common.math.matrix.SparseDoubleMatrix;
import com.lineate.internal.nltc.common.math.vector.HppcSparseDoubleVector;
import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;
import com.lineate.internal.nltc.common.pair.ImmutableIntDoublePair;
import org.apache.commons.lang3.NotImplementedException;
import org.la4j.matrix.ColumnMajorSparseMatrix;
import org.la4j.matrix.RowMajorSparseMatrix;
import org.la4j.matrix.sparse.CCSMatrix;
import org.la4j.matrix.sparse.CRSMatrix;

import javax.annotation.Nonnull;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class La4jCombinedSparseDoubleMatrix implements SparseDoubleMatrix {
    private final RowMajorSparseMatrix rowBasedMatrix;
    private final ColumnMajorSparseMatrix colBasedMatrix;

    public La4jCombinedSparseDoubleMatrix(int rowCount, int colCount) {
        rowBasedMatrix = new CRSMatrix(rowCount, colCount);
        colBasedMatrix = new CCSMatrix(rowCount, colCount);
    }

    public La4jCombinedSparseDoubleMatrix(SparseDoubleMatrix matrix) {
        rowBasedMatrix = new CRSMatrix(matrix.getRowCount(), matrix.getColCount());
        colBasedMatrix = new CCSMatrix(matrix.getRowCount(), matrix.getColCount());

        for (int row = 0, rowCount = matrix.getRowCount(); row < rowCount; ++row) {
            for (ImmutableIntDoublePair colAndValue : matrix.getSparseRow(row).getNonZeros()) {
                rowBasedMatrix.set(row, colAndValue.getLeft(), colAndValue.getRight());
            }
        }

        for (int col = 0, colCount = matrix.getColCount(); col < colCount; ++col) {
            for (ImmutableIntDoublePair rowAndValue : matrix.getSparseCol(col).getNonZeros()) {
                colBasedMatrix.set(rowAndValue.getLeft(), col, rowAndValue.getRight());
            }
        }
    }

    @Override
    public int getRowCount() {
        return rowBasedMatrix.rows();
    }

    @Override
    public int getColCount() {
        return rowBasedMatrix.columns();
    }

    @Override
    public void set(int row, int col, double value) {
        rowBasedMatrix.set(row, col, value);
        colBasedMatrix.set(row, col, value);
    }

    @Override
    public double get(int row, int col) {
        return rowBasedMatrix.get(row, col);
    }

    @Override
    public void clear() {
        throw new NotImplementedException("Unnecessary method for experimental matrix implementation.");
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseCol(int col) {
        SparseDoubleVector sparseCol = new HppcSparseDoubleVector(getRowCount());
        colBasedMatrix.eachNonZeroInColumn(col, sparseCol::set);
        return sparseCol;
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseRow(int row) {
        SparseDoubleVector sparseRow = new HppcSparseDoubleVector(getColCount());
        rowBasedMatrix.eachNonZeroInRow(row, sparseRow::set);
        return sparseRow;
    }
}
