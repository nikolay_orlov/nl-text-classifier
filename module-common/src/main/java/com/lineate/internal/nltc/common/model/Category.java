package com.lineate.internal.nltc.common.model;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

@Immutable
public class Category implements Comparable<Category> {
	@Nonnull
	private String classId;
	@Nonnull
	private double probability;
	
	public Category(@Nonnull String classId, double probability) {
		this.classId = classId;
		this.probability = probability;
	}
	
	public String getClassId() {
		return classId;
	}
	public double getProbability() {
		return probability;
	}
	@Override
	public int compareTo(Category c2) {
		return Double.compare(c2.getProbability(), this.getProbability());
	}
}
