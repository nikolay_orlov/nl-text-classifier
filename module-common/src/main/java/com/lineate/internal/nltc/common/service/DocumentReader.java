package com.lineate.internal.nltc.common.service;

import com.lineate.internal.nltc.common.model.Document;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Closeable;
import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface DocumentReader extends Closeable {
    @Nullable
    Document read() throws IOException;

    default void forEachRemaining(@Nonnull Consumer<Document> action) throws IOException {
        Objects.requireNonNull(action);

        Document document;

        while ((document = read()) != null) {
            action.accept(document);
        }
    }
}
