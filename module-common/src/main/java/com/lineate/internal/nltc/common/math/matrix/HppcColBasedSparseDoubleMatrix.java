package com.lineate.internal.nltc.common.math.matrix;

import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;

import javax.annotation.Nonnull;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class HppcColBasedSparseDoubleMatrix extends HppcBaseSparseDoubleMatrix implements ColBasedSparseDoubleMatrix {
    public HppcColBasedSparseDoubleMatrix(int rowCount, int colCount) {
        super(colCount, rowCount);
    }

    @Override
    public int getRowCount() {
        return getSecondaryKeyLength();
    }

    @Override
    public int getColCount() {
        return getPrimaryKeyLength();
    }

    @Override
    public void set(int row, int col, double value) {
        super.set(col, row, value);
    }

    @Override
    public double get(int row, int col) {
        return super.get(col, row);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseCol(int col) {
        return getSparseVector(col);
    }
}
