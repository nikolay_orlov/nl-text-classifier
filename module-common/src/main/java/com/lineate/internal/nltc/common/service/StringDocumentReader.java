package com.lineate.internal.nltc.common.service;

import com.lineate.internal.nltc.common.model.Document;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class StringDocumentReader implements DocumentReader {
    @Nonnull
    private final String text;

    @Nullable
    private final String documentCategory;

    private boolean completed;

    public StringDocumentReader(@Nonnull String text, @Nullable String documentCategory) {
        this.text = Objects.requireNonNull(text);
        this.documentCategory = StringUtils.trimToNull(documentCategory);
    }

    @Nullable
    @Override
    public Document read() {
        if (completed) {
            return null;
        }

        completed = true;

        return new Document(text, documentCategory);
    }

    @Override
    public void close() {
        completed = false;
    }
}
