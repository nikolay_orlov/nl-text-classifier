package com.lineate.internal.nltc.common.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.jetbrains.annotations.Contract;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Objects;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@Immutable
public final class Keyphrase {
    @Nonnull
    private final String text;

    public Keyphrase(@Nonnull String text) {
        this.text = Objects.requireNonNull(text);
    }

    @Contract(pure = true)
    @Nonnull
    public String getText() {
        return text;
    }

    @SuppressWarnings({"ControlFlowStatementWithoutBraces", "SimplifiableIfStatement"})
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Keyphrase keyphrase = (Keyphrase) o;

        return text.equals(keyphrase.text);
    }

    @Override
    public int hashCode() {
        return text.hashCode();
    }

    @Override
    public String toString() {
        return String.format("Keyphrase {text='%s'}", StringEscapeUtils.escapeJava(StringUtils.truncate(text, 100)));
    }
}
