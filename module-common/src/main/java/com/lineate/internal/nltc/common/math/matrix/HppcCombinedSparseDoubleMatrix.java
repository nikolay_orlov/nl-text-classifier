package com.lineate.internal.nltc.common.math.matrix;

import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;

import javax.annotation.Nonnull;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class HppcCombinedSparseDoubleMatrix implements SparseDoubleMatrix {
    private final RowBasedSparseDoubleMatrix rowBasedMatrix;
    private final ColBasedSparseDoubleMatrix colBasedMatrix;

    public HppcCombinedSparseDoubleMatrix(int rowCount, int colCount) {
        rowBasedMatrix = new HppcRowBasedSparseDoubleMatrix(rowCount, colCount);
        colBasedMatrix = new HppcColBasedSparseDoubleMatrix(rowCount, colCount);
    }

    @Override
    public int getRowCount() {
        return rowBasedMatrix.getRowCount();
    }

    @Override
    public int getColCount() {
        return rowBasedMatrix.getColCount();
    }

    @Override
    public void set(int row, int col, double value) {
        rowBasedMatrix.set(row, col, value);
        colBasedMatrix.set(row, col, value);
    }

    @Override
    public double get(int row, int col) {
        return rowBasedMatrix.get(row, col);
    }

    @Override
    public void clear() {
        rowBasedMatrix.clear();
        colBasedMatrix.clear();
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseCol(int col) {
        return colBasedMatrix.getSparseCol(col);
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseRow(int row) {
        return rowBasedMatrix.getSparseRow(row);
    }
}
