package com.lineate.internal.nltc.common.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.jetbrains.annotations.Contract;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Objects;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@Immutable
public final class Document {
    @Nonnull
    private final String text;

    @Nullable
    private final String category;

    public Document(@Nonnull String text, @Nullable String category) {
        this.text = Objects.requireNonNull(text);
        this.category = StringUtils.trimToNull(category);
    }

    @Contract(pure = true)
    @Nonnull
    public String getText() {
        return text;
    }

    @Contract(pure = true)
    @Nullable
    public String getCategory() {
        return category;
    }

    @SuppressWarnings({"ControlFlowStatementWithoutBraces", "SimplifiableIfStatement"})
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Document document = (Document) o;

        if (!text.equals(document.text)) return false;
        return category != null ? category.equals(document.category) : document.category == null;
    }

    @Override
    public int hashCode() {
        int result = text.hashCode();
        result = 31 * result + (category != null ? category.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(
                "Document {category='%s', text='%s'}",
                category, StringEscapeUtils.escapeJava(StringUtils.truncate(text, 100))
        );
    }
}
