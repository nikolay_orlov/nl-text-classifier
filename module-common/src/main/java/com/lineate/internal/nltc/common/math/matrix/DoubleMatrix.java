package com.lineate.internal.nltc.common.math.matrix;

import javax.annotation.Nonnegative;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface DoubleMatrix {
    int getRowCount();
    int getColCount();

    void set(@Nonnegative int row, @Nonnegative int col, double value);
    double get(@Nonnegative int row, @Nonnegative int col);

    void clear();
}
