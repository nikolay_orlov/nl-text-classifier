package com.lineate.internal.nltc.common.math.matrix;

import com.carrotsearch.hppc.*;
import com.carrotsearch.hppc.procedures.IntDoubleProcedure;
import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.math.vector.HppcSparseDoubleVector;
import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@SuppressWarnings("AbstractClassWithoutAbstractMethods")
abstract class HppcBaseSparseDoubleMatrix {
    private final IntObjectMap<IntDoubleMap> valueBySecondaryKeyByPrimaryKey;
    private final int primaryKeyLength;
    private final int secondaryKeyLength;

    protected HppcBaseSparseDoubleMatrix(int primaryKeyLength, int secondaryKeyLength) {
        Preconditions.checkArgument(primaryKeyLength > 0);
        Preconditions.checkArgument(secondaryKeyLength > 0);

        this.valueBySecondaryKeyByPrimaryKey = new IntObjectHashMap<>();
        this.primaryKeyLength = primaryKeyLength;
        this.secondaryKeyLength = secondaryKeyLength;
    }

    protected int getPrimaryKeyLength() {
        return primaryKeyLength;
    }

    protected int getSecondaryKeyLength() {
        return secondaryKeyLength;
    }

    protected void set(int primaryKey, int secondaryKey, double value) {
        Preconditions.checkArgument(primaryKey >= 0 && primaryKey < primaryKeyLength);
        Preconditions.checkArgument(secondaryKey >= 0 && secondaryKey < secondaryKeyLength);

        if (Double.compare(value, 0.0D) == 0) {
            IntDoubleMap valueBySecondaryKey = valueBySecondaryKeyByPrimaryKey.get(primaryKey);

            if (valueBySecondaryKey != null) {
                valueBySecondaryKey.remove(secondaryKey);

                if (valueBySecondaryKey.isEmpty()) {
                    valueBySecondaryKeyByPrimaryKey.remove(primaryKey);
                }
            }
        } else {
            IntDoubleMap valueBySecondaryKey = valueBySecondaryKeyByPrimaryKey.get(primaryKey);

            if (valueBySecondaryKey == null) {
                valueBySecondaryKey = new IntDoubleHashMap();
                valueBySecondaryKeyByPrimaryKey.put(primaryKey, valueBySecondaryKey);
            }

            valueBySecondaryKey.put(secondaryKey, value);
        }
    }

    protected double get(int primaryKey, int secondaryKey) {
        Preconditions.checkArgument(primaryKey >= 0 && primaryKey < primaryKeyLength);
        Preconditions.checkArgument(secondaryKey >= 0 && secondaryKey < secondaryKeyLength);

        IntDoubleMap valueBySecondaryKey = valueBySecondaryKeyByPrimaryKey.get(primaryKey);

        return valueBySecondaryKey == null ? 0.0D : valueBySecondaryKey.getOrDefault(secondaryKey, 0.0D);
    }

    protected void clear() {
        valueBySecondaryKeyByPrimaryKey.clear();
    }

    protected SparseDoubleVector getSparseVector(int primaryKey) {
        SparseDoubleVector sparseVector = new HppcSparseDoubleVector(secondaryKeyLength);

        IntDoubleMap valueBySecondaryKey = valueBySecondaryKeyByPrimaryKey.get(primaryKey);

        if (valueBySecondaryKey != null) {
            valueBySecondaryKey.forEach((IntDoubleProcedure) sparseVector::set);
        }

        return sparseVector;
    }
}
