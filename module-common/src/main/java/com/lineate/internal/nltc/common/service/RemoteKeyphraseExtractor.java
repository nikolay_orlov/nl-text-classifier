package com.lineate.internal.nltc.common.service;

import com.google.common.base.Preconditions;
import com.google.gson.*;
import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.model.Keyphrase;
import org.apache.commons.text.StringEscapeUtils;
import ru.sladethe.common.io.MimeType;
import ru.sladethe.common.io.http.*;
import ru.sladethe.common.text.UrlUtil;
import ru.sladethe.common.time.TimeUtil;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@ThreadSafe
public class RemoteKeyphraseExtractor implements KeyphraseExtractor {
    private static final ThreadLocal<Gson> threadLocalGson = ThreadLocal.withInitial(() -> new GsonBuilder().create());

    private final String remoteUrl;

    public RemoteKeyphraseExtractor(@Nonnull String remoteUrl) {
        Preconditions.checkArgument(UrlUtil.isValidUrl(remoteUrl));
        this.remoteUrl = remoteUrl;
    }

    @Nonnull
    @Override
    public List<Keyphrase> extract(@Nonnull Document document) throws IOException {
        String requestBody = String.format(
                "{\"text\": \"%s\", \"keywords-limit\": 20, \"relevance-threshold\": 0.999}",
                StringEscapeUtils.escapeJson(document.getText())
        );

        HttpResponse response = HttpUtil.newRequest(remoteUrl)
                .setMethod(HttpMethod.POST)
                .setTimeoutMillis(2L * TimeUtil.MILLIS_PER_SECOND)
                .setBinaryEntity(requestBody.getBytes(UTF_8))
                .appendHeader("Content-Type", MimeType.APPLICATION_JSON)
                .executeAndReturnResponse()
                .throwIoException();

        try {
            Gson gson = threadLocalGson.get();
            JsonObject rootJsonObject = gson.fromJson(response.getUtf8String(), JsonElement.class).getAsJsonObject();
            Keyphrase[] keyphrases = gson.fromJson(rootJsonObject.getAsJsonArray("keywords"), Keyphrase[].class);
            return Arrays.asList(keyphrases);
        } catch (RuntimeException e) {
            throw new IOException(String.format(
                    "Can't parse JSON received from '%s': '%s'.",
                    remoteUrl, StringEscapeUtils.unescapeHtml4(response.getUtf8String())
            ), e);
        }
    }
}
