package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.math.matrix.DoubleMatrix;
import gnu.trove.impl.Constants;
import gnu.trove.map.TLongDoubleMap;
import gnu.trove.map.hash.TLongDoubleHashMap;
import ru.sladethe.common.math.NumberUtil;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class TroveHashMapLongKeySparseDoubleMatrix implements DoubleMatrix {
    private final TLongDoubleMap valueByPackedRowCol = new TLongDoubleHashMap(
            Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, 0L, 0.0D
    );

    private final int rowCount;
    private final int colCount;

    public TroveHashMapLongKeySparseDoubleMatrix(int rowCount, int colCount) {
        Preconditions.checkArgument(rowCount > 0);
        Preconditions.checkArgument(colCount > 0);

        this.rowCount = rowCount;
        this.colCount = colCount;
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColCount() {
        return colCount;
    }

    @Override
    public void set(int row, int col, double value) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        long packedRowCol = NumberUtil.packInts(row, col);

        if (Double.compare(value, 0.0D) == 0) {
            valueByPackedRowCol.remove(packedRowCol);
        } else {
            valueByPackedRowCol.put(packedRowCol, value);
        }
    }

    @Override
    public double get(int row, int col) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        long packedRowCol = NumberUtil.packInts(row, col);

        return valueByPackedRowCol.get(packedRowCol);
    }

    @Override
    public void clear() {
        valueByPackedRowCol.clear();
    }
}
