package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.math.matrix.DoubleMatrix;
import org.apache.commons.lang3.ObjectUtils;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class JavaMapSparseDoubleMatrix implements DoubleMatrix {
    private final Map<Integer, Map<Integer, Double>> valueByColByRow;
    private final Supplier<Map<Integer, Double>> newMapSupplier;
    private final int rowCount;
    private final int colCount;
    private final Double defaultValue;

    public JavaMapSparseDoubleMatrix(
            int rowCount, int colCount, double defaultValue,
            @Nonnull Map<Integer, Map<Integer, Double>> valueByColByRow,
            @Nonnull Supplier<Map<Integer, Double>> newMapSupplier) {
        Preconditions.checkArgument(rowCount > 0);
        Preconditions.checkArgument(colCount > 0);

        this.valueByColByRow = Objects.requireNonNull(valueByColByRow);
        this.newMapSupplier = Objects.requireNonNull(newMapSupplier);
        this.rowCount = rowCount;
        this.colCount = colCount;
        this.defaultValue = defaultValue;
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColCount() {
        return colCount;
    }

    @Override
    public void set(int row, int col, double value) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        forceSet(valueByColByRow, col, row, value, newMapSupplier, defaultValue);
    }

    @Override
    public double get(int row, int col) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        Map<Integer, Double> valueByCol = valueByColByRow.get(row);

        return valueByCol == null ? defaultValue : ObjectUtils.defaultIfNull(valueByCol.get(col), defaultValue);
    }

    @Override
    public void clear() {
        valueByColByRow.clear();
    }

    static void forceSet(
            @Nonnull Map<Integer, Map<Integer, Double>> valueByKeyAByKeyB, int keyA, int keyB, double value,
            @Nonnull Supplier<Map<Integer, Double>> newMapSupplier, @Nonnull Double defaultValue) {
        if (defaultValue.equals(value)) {
            Map<Integer, Double> valueByKeyA = valueByKeyAByKeyB.get(keyB);

            if (valueByKeyA != null) {
                valueByKeyA.remove(keyA);

                if (valueByKeyA.isEmpty()) {
                    valueByKeyAByKeyB.remove(keyB);
                }
            }
        } else {
            valueByKeyAByKeyB.computeIfAbsent(keyB, __ -> newMapSupplier.get()).put(keyA, value);
        }
    }
}
