package com.lineate.internal.nltc.common.math.vector;

import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.pair.ImmutableIntDoublePair;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface SparseDoubleVector extends DoubleVector {
    /**
     * @return index-value pairs of all non-zero elements, ordered by index
     */
    @Nonnull
    List<ImmutableIntDoublePair> getNonZeros();

    /**
     * @param sparseVector other vector to dot-product
     * @return dot product of this and provided sparse vector
     */
    default double dotProduct(@Nonnull SparseDoubleVector sparseVector) {
        Preconditions.checkArgument(getLength() == sparseVector.getLength());

        List<ImmutableIntDoublePair> nonZeros = getNonZeros();
        List<ImmutableIntDoublePair> otherNonZeros = sparseVector.getNonZeros();

        int pos = 0;
        int otherPos = 0;

        double dotProduct = 0.0D;

        while (pos < nonZeros.size() && otherPos < otherNonZeros.size()) {
            while (pos < nonZeros.size() && otherPos < otherNonZeros.size()
                    && nonZeros.get(pos).getLeft() != otherNonZeros.get(otherPos).getLeft()) {
                int otherIndex = otherNonZeros.get(otherPos).getLeft();

                while (pos < nonZeros.size() && nonZeros.get(pos).getLeft() < otherIndex) {
                    ++pos;
                }

                if (pos < nonZeros.size()) {
                    int index = nonZeros.get(pos).getLeft();

                    while (otherPos < otherNonZeros.size() && otherNonZeros.get(otherPos).getLeft() < index) {
                        ++otherPos;
                    }
                }
            }

            if (pos < nonZeros.size() && otherPos < otherNonZeros.size()) {
                dotProduct += nonZeros.get(pos++).getRight() * otherNonZeros.get(otherPos++).getRight();
            }
        }

        return dotProduct;
    }
}
