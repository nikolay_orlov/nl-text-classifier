package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.math.matrix.DoubleMatrix;
import org.apache.commons.lang3.ObjectUtils;
import ru.sladethe.common.math.NumberUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class HashMapLongKeySparseDoubleMatrix implements DoubleMatrix {
    private final Map<Long, Double> valueByPackedRowCol = new HashMap<>();
    private final int rowCount;
    private final int colCount;
    private final Double defaultValue;

    public HashMapLongKeySparseDoubleMatrix(int rowCount, int colCount, double defaultValue) {
        Preconditions.checkArgument(rowCount > 0);
        Preconditions.checkArgument(colCount > 0);

        this.rowCount = rowCount;
        this.colCount = colCount;
        this.defaultValue = defaultValue;
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColCount() {
        return colCount;
    }

    @Override
    public void set(int row, int col, double value) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        long packedRowCol = NumberUtil.packInts(row, col);

        if (defaultValue.equals(value)) {
            valueByPackedRowCol.remove(packedRowCol);
        } else {
            valueByPackedRowCol.put(packedRowCol, value);
        }
    }

    @Override
    public double get(int row, int col) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        long packedRowCol = NumberUtil.packInts(row, col);

        return ObjectUtils.defaultIfNull(valueByPackedRowCol.get(packedRowCol), defaultValue);
    }

    @Override
    public void clear() {
        valueByPackedRowCol.clear();
    }
}
