package com.lineate.internal.nltc.common.service;

import com.carrotsearch.hppc.ObjectIntHashMap;
import com.carrotsearch.hppc.ObjectIntMap;
import com.carrotsearch.hppc.cursors.ObjectIntCursor;
import com.carrotsearch.hppc.procedures.ObjectIntProcedure;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lineate.internal.nltc.common.math.matrix.*;
import com.lineate.internal.nltc.common.math.vector.HppcSparseDoubleVector;
import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;
import com.lineate.internal.nltc.common.model.Category;
import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.model.Keyphrase;
import com.lineate.internal.nltc.common.pair.ImmutableIntDoublePair;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.Contract;

import ru.sladethe.common.lang.ObjectUtil;

import javax.annotation.*;
import javax.annotation.concurrent.NotThreadSafe;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@NotThreadSafe
public final class ClassifierImpl implements Classifier {
    private final Set<String> documentHashes = new HashSet<>();

    private final ObjectIntMap<Keyphrase> indexByKeyphrase = new ObjectIntHashMap<>();
    private final ObjectIntMap<String> indexByCategory = new ObjectIntHashMap<>();

    private final ColBasedSparseDoubleMatrix documentKeyphraseRelations = new HppcColBasedSparseDoubleMatrix(
            Integer.MAX_VALUE, Integer.MAX_VALUE
    );

    private final ColBasedSparseDoubleMatrix documentCategoryRelations = new HppcColBasedSparseDoubleMatrix(
            Integer.MAX_VALUE, Integer.MAX_VALUE
    );

    private int documentCount;

    @Nullable
    private RowBasedSparseDoubleMatrix classificationMatrix;

    public boolean isRegistered(@Nonnull Document document) {
        return isRegistered(getDocumentHash(document));
    }

    @Contract(pure = true)
    private boolean isRegistered(@Nonnull String documentHash) {
        return documentHashes.contains(documentHash);
    }

    public void register(@Nonnull Document document, @Nonnull List<Keyphrase> keyphrases) {
        Objects.requireNonNull(document);
        Objects.requireNonNull(keyphrases);

        String documentHash = getDocumentHash(document);

        if (isRegistered(documentHash)) {
            return;
        }

        documentHashes.add(documentHash);

        int categoryIndex = addCategoryAndGetIndex(Objects.requireNonNull(document.getCategory()));
        setDocumentCategoryRelation(documentCount, categoryIndex, true);

        for (Keyphrase keyphrase : keyphrases) {
            int keyphraseIndex = addKeyphraseAndGetIndex(keyphrase);
            setDocumentKeyphraseRelation(documentCount, keyphraseIndex, true);
        }

        ++documentCount;
    }

    @Override
    public List<Category> classify(
    		List<Keyphrase> keyphrases, double probability) {
    	Objects.requireNonNull(keyphrases);

        SparseDoubleVector documentKeyphrasesVector = new HppcSparseDoubleVector(indexByKeyphrase.size());

        for (Keyphrase keyphrase : keyphrases) {
            int keyphraseIndex = indexByKeyphrase.getOrDefault(Objects.requireNonNull(keyphrase), -1);
            if (keyphraseIndex >= 0) {
                documentKeyphrasesVector.set(keyphraseIndex, 1.0D);
            }
        }

        @SuppressWarnings("LocalVariableHidesMemberVariable")
        RowBasedSparseDoubleMatrix classificationMatrix = calculateAndGetClassificationMatrix();

        List<Category> result = new ArrayList<>();
        double weightSum = 0;
        for (int categoryIndex = 0; categoryIndex < indexByCategory.size(); ++categoryIndex) {
        	weightSum += classificationMatrix.getSparseRow(categoryIndex)
                    .dotProduct(documentKeyphrasesVector);
        }
        for (int categoryIndex = 0; categoryIndex < indexByCategory.size(); ++categoryIndex) {
        	double categoryProbability = classificationMatrix.getSparseRow(categoryIndex)
                    .dotProduct(documentKeyphrasesVector) / weightSum;
        	if (categoryProbability >= probability) {
        		result.add(new Category(getCategoryByIndex(categoryIndex), categoryProbability));
        	}
        }
        Collections.sort(result);
    	return result;
    }
    
    protected String getCategoryByIndex(int categoryIndex) {
    	for (ObjectIntCursor<String> categoryAndIndex : indexByCategory) {
            if (categoryAndIndex.value == categoryIndex) {
                return categoryAndIndex.key;
            }
        }
    	return null;
    }
    
    @Nullable
    @Override
    public String classify(@Nonnull List<Keyphrase> keyphrases) {
        Objects.requireNonNull(keyphrases);

        SparseDoubleVector documentKeyphrasesVector = new HppcSparseDoubleVector(indexByKeyphrase.size());

        for (Keyphrase keyphrase : keyphrases) {
            int keyphraseIndex = indexByKeyphrase.getOrDefault(Objects.requireNonNull(keyphrase), -1);
            if (keyphraseIndex >= 0) {
                documentKeyphrasesVector.set(keyphraseIndex, 1.0D);
            }
        }

        @SuppressWarnings("LocalVariableHidesMemberVariable")
        RowBasedSparseDoubleMatrix classificationMatrix = calculateAndGetClassificationMatrix();

        double[] categoryWeights = new double[indexByCategory.size()];

        for (int categoryIndex = 0; categoryIndex < indexByCategory.size(); ++categoryIndex) {
            categoryWeights[categoryIndex] = classificationMatrix.getSparseRow(categoryIndex)
                    .dotProduct(documentKeyphrasesVector);
        }

        // To get the category probabilities we should divide all weights by this summary weight.
        // But now we only search for the max weight index, so division of all elements doesn't change anything.
        @SuppressWarnings("unused") double summaryWeight = Arrays.stream(categoryWeights).sum();

        double maxWeight = Arrays.stream(categoryWeights).max().orElse(0.0D);

        int categoryIndex = ArrayUtils.indexOf(categoryWeights, maxWeight);

        for (ObjectIntCursor<String> categoryAndIndex : indexByCategory) {
            if (categoryAndIndex.value == categoryIndex) {
                return categoryAndIndex.key;
            }
        }

        return null;
    }

    @Nonnegative
    private int addCategoryAndGetIndex(@Nonnull String category) {
        int categoryIndex = indexByCategory.getOrDefault(Objects.requireNonNull(category), -1);
        if (categoryIndex < 0) {
            indexByCategory.put(category, categoryIndex = indexByCategory.size());
            classificationMatrix = null;
        }
        return categoryIndex;
    }

    private void setDocumentCategoryRelation(int documentIndex, int categoryIndex, boolean relation) {
        documentCategoryRelations.set(documentIndex, categoryIndex, relation ? 1.0D : 0.0D);
        classificationMatrix = null;
    }

    @Nonnegative
    private int addKeyphraseAndGetIndex(@Nonnull Keyphrase keyphrase) {
        int keyphraseIndex = indexByKeyphrase.getOrDefault(Objects.requireNonNull(keyphrase), -1);
        if (keyphraseIndex < 0) {
            indexByKeyphrase.put(keyphrase, keyphraseIndex = indexByKeyphrase.size());
            classificationMatrix = null;
        }
        return keyphraseIndex;
    }

    private void setDocumentKeyphraseRelation(int documentIndex, int keyphraseIndex, boolean relation) {
        documentKeyphraseRelations.set(documentIndex, keyphraseIndex, relation ? 1.0D : 0.0D);
        classificationMatrix = null;
    }

    @Nonnull
    public RowBasedSparseDoubleMatrix calculateAndGetClassificationMatrix() {
        RowBasedSparseDoubleMatrix localClassificationMatrix = classificationMatrix;

        if (localClassificationMatrix == null) {
            classificationMatrix = localClassificationMatrix = forceCalculateClassificationMatrix();
        }

        return localClassificationMatrix;
    }

    @Nonnull
    private RowBasedSparseDoubleMatrix forceCalculateClassificationMatrix() {
        int categoryCount = indexByCategory.size();
        int keyphraseCount = indexByKeyphrase.size();

        RowBasedSparseDoubleMatrix localClassificationMatrix = new HppcRowBasedSparseDoubleMatrix(
                categoryCount, keyphraseCount
        );

        for (int categoryIndex = 0; categoryIndex < categoryCount; ++categoryIndex) {
            SparseDoubleVector categoryCol = documentCategoryRelations.getSparseCol(categoryIndex);
            double categoryDocumentCount = categoryCol.getNonZeros().size();

            for (int keyphraseIndex = 0; keyphraseIndex < keyphraseCount; ++keyphraseIndex) {
                SparseDoubleVector keyphraseCol = documentKeyphraseRelations.getSparseCol(keyphraseIndex);
                double keyphraseDocumentCount = keyphraseCol.getNonZeros().size();

                localClassificationMatrix.set(
                        categoryIndex, keyphraseIndex,
                        categoryCol.dotProduct(keyphraseCol) / (categoryDocumentCount * keyphraseDocumentCount)
                );
            }
        }

        return localClassificationMatrix;
    }

    @SuppressWarnings("OverlyLongMethod")
    @Nonnull
    public byte[] save() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        writeDouble(outputStream, 1.0D); // Version.
        writeInt(outputStream, documentCount);

        writeInt(outputStream, documentHashes.size());

        documentHashes.forEach(documentHash -> {
            try {
                byte[] bytes = Hex.decodeHex(documentHash);
                writeInt(outputStream, bytes.length);
                writeBytes(outputStream, bytes);
            } catch (DecoderException e) {
                throw new IllegalStateException("Document hash should be a correct hex-string.", e);
            }
        });

        int keyphraseCount = indexByKeyphrase.size();
        writeInt(outputStream, keyphraseCount);

        indexByKeyphrase.forEach((ObjectIntProcedure<Keyphrase>) (keyphrase, index) -> {
            writeString(outputStream, keyphrase.getText());
            writeInt(outputStream, index);
        });

        int categoryCount = indexByCategory.size();
        writeInt(outputStream, categoryCount);

        indexByCategory.forEach((ObjectIntProcedure<String>) (category, index) -> {
            writeString(outputStream, category);
            writeInt(outputStream, index);
        });

        writeDocumentEntityRelations(outputStream, documentKeyphraseRelations, keyphraseCount);
        writeDocumentEntityRelations(outputStream, documentCategoryRelations, categoryCount);

        if (classificationMatrix == null) {
            writeBytes(outputStream, new byte[] {0});
        } else {
            writeBytes(outputStream, new byte[] {1});

            for (int categoryIndex = 0; categoryIndex < categoryCount; ++categoryIndex) {
                List<ImmutableIntDoublePair> nonZeroCols = classificationMatrix.getSparseRow(categoryIndex)
                        .getNonZeros();

                writeInt(outputStream, nonZeroCols.size());

                nonZeroCols.forEach(colAndWeight -> {
                    writeInt(outputStream, colAndWeight.getLeft());
                    writeDouble(outputStream, colAndWeight.getRight());
                });
            }
        }

        return outputStream.toByteArray();
    }

    @SuppressWarnings("OverlyLongMethod")
    public void load(@Nonnull byte[] data) {
        Objects.requireNonNull(data);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);

        Preconditions.checkArgument(Double.compare(readDouble(inputStream), 1.0D) == 0); // Version.
        documentCount = readInt(inputStream);

        int documentHashCount = readInt(inputStream);
        documentHashes.clear();

        for (int hashIndex = 0; hashIndex < documentHashCount; ++hashIndex) {
            int hashLength = readInt(inputStream);
            documentHashes.add(Hex.encodeHexString(readBytes(inputStream, hashLength)));
        }

        int keyphraseCount = readInt(inputStream);
        indexByKeyphrase.clear();

        for (int keyphraseIndex = 0; keyphraseIndex < keyphraseCount; ++keyphraseIndex) {
            indexByKeyphrase.put(new Keyphrase(Objects.requireNonNull(readString(inputStream))), readInt(inputStream));
        }

        int categoryCount = readInt(inputStream);
        indexByCategory.clear();

        for (int categoryIndex = 0; categoryIndex < categoryCount; ++categoryIndex) {
            indexByCategory.put(readString(inputStream), readInt(inputStream));
        }

        readDocumentEntityRelations(inputStream, documentKeyphraseRelations, keyphraseCount);
        readDocumentEntityRelations(inputStream, documentCategoryRelations, categoryCount);

        if (readBytes(inputStream, 1)[0] == 0) {
            classificationMatrix = null;
        } else {
            classificationMatrix = new HppcRowBasedSparseDoubleMatrix(categoryCount, keyphraseCount);

            for (int categoryIndex = 0; categoryIndex < categoryCount; ++categoryIndex) {
                int nonZeroColCount = readInt(inputStream);

                for (int i = 0; i < nonZeroColCount; ++i) {
                    classificationMatrix.set(categoryIndex, readInt(inputStream), readDouble(inputStream));
                }
            }
        }
    }

    private static void writeBytes(OutputStream outputStream, byte[] bytes) {
        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Can't write %d bytes to stream.", bytes.length), e);
        }
    }

    private static void writeInt(OutputStream outputStream, int value) {
        writeBytes(outputStream, ByteBuffer.allocate(Integer.SIZE / Byte.SIZE).putInt(value).array());
    }

    private static void writeLong(OutputStream outputStream, long value) {
        writeBytes(outputStream, ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(value).array());
    }

    private static void writeDouble(OutputStream outputStream, double value) {
        writeLong(outputStream, Double.doubleToLongBits(value));
    }

    private static void writeString(OutputStream outputStream, @Nullable String value) {
        if (value == null) {
            writeInt(outputStream, -1);
            return;
        }

        byte[] bytes = value.getBytes(UTF_8);

        writeInt(outputStream, bytes.length);
        writeBytes(outputStream, bytes);
    }

    private static void writeDocumentEntityRelations(
            OutputStream outputStream, ColBasedSparseDoubleMatrix documentEntityRelations, int entityCount) {
        for (int entityIndex = 0; entityIndex < entityCount; ++entityIndex) {
            int[] documentIndices = documentEntityRelations.getSparseCol(entityIndex).getNonZeros().stream()
                    .mapToInt(ImmutableIntDoublePair::getLeft).toArray();

            writeInt(outputStream, documentIndices.length);

            for (int documentIndex : documentIndices) {
                writeInt(outputStream, documentIndex);
            }
        }
    }

    private static byte[] readBytes(InputStream inputStream, int byteCount) {
        byte[] bytes = new byte[byteCount];
        int offset = 0;

        try {
            int readByteCount;

            while (offset < byteCount && (readByteCount = inputStream.read(bytes, offset, byteCount - offset)) != -1) {
                offset += readByteCount;
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("Can't read %d bytes from stream.", byteCount), e);
        }

        if (offset != byteCount) {
            throw new RuntimeException(String.format("Can't read %d bytes from stream.", byteCount));
        }

        return bytes;
    }

    private static int readInt(InputStream inputStream) {
        return ByteBuffer.wrap(readBytes(inputStream, Integer.SIZE / Byte.SIZE)).getInt();
    }

    private static long readLong(InputStream inputStream) {
        return ByteBuffer.wrap(readBytes(inputStream, Long.SIZE / Byte.SIZE)).getLong();
    }

    private static double readDouble(InputStream inputStream) {
        return Double.longBitsToDouble(readLong(inputStream));
    }

    @Nullable
    private static String readString(InputStream inputStream) {
        int length = readInt(inputStream);
        return length == -1 ? null : new String(readBytes(inputStream, length), UTF_8);
    }

    private static void readDocumentEntityRelations(
            InputStream inputStream, @Nonnull ColBasedSparseDoubleMatrix documentEntityRelations, int entityCount) {
        documentEntityRelations.clear();

        for (int entityIndex = 0; entityIndex < entityCount; ++entityIndex) {
            int documentIndexCount = readInt(inputStream);

            for (int i = 0; i < documentIndexCount; ++i) {
                int documentIndex = readInt(inputStream);
                documentEntityRelations.set(documentIndex, entityIndex, 1.0D);
            }
        }
    }

    @Nonnull
    private static String getDocumentHash(@Nonnull Document document) {
        Objects.requireNonNull(document);

        JsonObject documentObject = new JsonObject();
        documentObject.addProperty("text", document.getText());
        ObjectUtil.ifNotNull(document.getCategory(), category -> documentObject.addProperty("category", category));
        return DigestUtils.sha512Hex(new Gson().toJson(documentObject));
    }
}
