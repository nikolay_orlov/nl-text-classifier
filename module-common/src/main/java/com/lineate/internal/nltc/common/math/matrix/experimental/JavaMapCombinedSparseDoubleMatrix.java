package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.math.matrix.SparseDoubleMatrix;
import com.lineate.internal.nltc.common.math.vector.HppcSparseDoubleVector;
import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;
import org.apache.commons.lang3.ObjectUtils;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Supplier;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class JavaMapCombinedSparseDoubleMatrix implements SparseDoubleMatrix {
    private final Map<Integer, Map<Integer, Double>> valueByColByRow;
    private final Map<Integer, Map<Integer, Double>> valueByRowByCol;
    private final Supplier<Map<Integer, Double>> newMapSupplier;
    private final int rowCount;
    private final int colCount;
    private final Double defaultValue;

    public JavaMapCombinedSparseDoubleMatrix(int rowCount, int colCount, double defaultValue,
                                             @Nonnull Map<Integer, Map<Integer, Double>> valueByColByRow,
                                             @Nonnull Map<Integer, Map<Integer, Double>> valueByRowByCol,
                                             @Nonnull Supplier<Map<Integer, Double>> newMapSupplier) {
        Preconditions.checkArgument(rowCount > 0);
        Preconditions.checkArgument(colCount > 0);

        this.valueByColByRow = Objects.requireNonNull(valueByColByRow);
        this.valueByRowByCol = Objects.requireNonNull(valueByRowByCol);
        this.newMapSupplier = Objects.requireNonNull(newMapSupplier);
        this.rowCount = rowCount;
        this.colCount = colCount;
        this.defaultValue = defaultValue;
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColCount() {
        return colCount;
    }

    @Override
    public void set(int row, int col, double value) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        JavaMapSparseDoubleMatrix.forceSet(valueByColByRow, col, row, value, newMapSupplier, defaultValue);
        JavaMapSparseDoubleMatrix.forceSet(valueByRowByCol, row, col, value, newMapSupplier, defaultValue);
    }

    @Override
    public double get(int row, int col) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        Map<Integer, Double> valueByCol = valueByColByRow.get(row);

        return valueByCol == null ? defaultValue : ObjectUtils.defaultIfNull(valueByCol.get(col), defaultValue);
    }

    @Override
    public void clear() {
        valueByColByRow.clear();
        valueByRowByCol.clear();
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseCol(int col) {
        SparseDoubleVector sparseCol = new HppcSparseDoubleVector(rowCount);
        Optional.ofNullable(valueByRowByCol.get(col)).ifPresent(valueByRow -> valueByRow.forEach(sparseCol::set));
        return sparseCol;
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseRow(int row) {
        SparseDoubleVector sparseRow = new HppcSparseDoubleVector(colCount);
        Optional.ofNullable(valueByColByRow.get(row)).ifPresent(valueByCol -> valueByCol.forEach(sparseRow::set));
        return sparseRow;
    }
}
