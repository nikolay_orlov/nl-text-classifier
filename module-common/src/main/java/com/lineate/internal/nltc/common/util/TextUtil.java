package com.lineate.internal.nltc.common.util;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import ru.sladethe.common.text.StringUtil;

import javax.annotation.Nonnull;
import javax.annotation.RegEx;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public final class TextUtil {
    private static final String WHITESPACE_CHARS = "" // Dummy empty string for homogeneity
            + '\u0009' // CHARACTER TABULATION
            + '\n' // LINE FEED (LF)
            + '\u000B' // LINE TABULATION
            + '\u000C' // FORM FEED (FF)
            + '\r' // CARRIAGE RETURN (CR)
            + '\u0020' // SPACE
            + '\u0085' // NEXT LINE (NEL)
            + '\u00A0' // NO-BREAK SPACE
            + '\u1680' // OGHAM SPACE MARK
            + '\u180E' // MONGOLIAN VOWEL SEPARATOR
            + '\u2000' // EN QUAD
            + '\u2001' // EM QUAD
            + '\u2002' // EN SPACE
            + '\u2003' // EM SPACE
            + '\u2004' // THREE-PER-EM SPACE
            + '\u2005' // FOUR-PER-EM SPACE
            + '\u2006' // SIX-PER-EM SPACE
            + '\u2007' // FIGURE SPACE
            + '\u2008' // PUNCTUATION SPACE
            + '\u2009' // THIN SPACE
            + '\u200A' // HAIR SPACE
            + '\u2028' // LINE SEPARATOR
            + '\u2029' // PARAGRAPH SEPARATOR
            + '\u202F' // NARROW NO-BREAK SPACE
            + '\u205F' // MEDIUM MATHEMATICAL SPACE
            + '\u3000' // IDEOGRAPHIC SPACE
            ;

    @RegEx
    private static final String REGEXP_DASH_CHARS = "\\-\\-";

    @RegEx
    private static final String REGEXP_DASH_CHARS_CLASS = '[' + REGEXP_DASH_CHARS + ']';

    @RegEx
    private static final String REGEXP_ENG_WORD = "[a-z][a-z']*(" + REGEXP_DASH_CHARS_CLASS + "[a-z']+)*";

    @RegEx
    private static final String REGEXP_ENG_WORD2 = "[a-zA-Z][a-z']*(" + REGEXP_DASH_CHARS_CLASS + "[A-Z])?[a-z']+";

    @RegEx
    private static final String REGEXP_ENG_COMMON_WORDS = "i|a|i\\.e\\.|e\\.g\\.";

    @RegEx
    private static final String REGEXP_SLASHED_ENG_WORDS = REGEXP_ENG_WORD + '/' + REGEXP_ENG_WORD;

    @RegEx
    private static final String REGEXP_ENG_ABBR = "([A-Z][a-z]*){2,}";

    @RegEx
    private static final String REGEXP_ENG_DASHED_ABBR = "([A-Z]*[a-z]*" + REGEXP_DASH_CHARS_CLASS + "){1,}[A-Z]+";

    @RegEx
    private static final String REGEXP_ENG_DASHED_ABBR2 = "([A-Z]+" + REGEXP_DASH_CHARS_CLASS + "){1,}[A-Z]+[a-z]+";

    @RegEx
    private static final String REGEXP_ENG_ABBR_WITH_NUMBER = "([A-Z][a-z]*" + REGEXP_DASH_CHARS_CLASS + "?){1,}(\\d)+";


    private static final List<Pair<Pattern, String>> DOCUMENT_PRETTIFY_PATTERN_AND_REPLACEMENT_PAIRS = Arrays.asList(
            new ImmutablePair<>(Pattern.compile(
                    "((^|\\b)<?(([a-zA-Z0-9\\-+_=]+\\$)?[a-zA-Z0-9\\-+_=]+(\\.[a-zA-Z0-9\\-+_=]+)*@" +
                            "[a-zA-Z0-9\\-+_=]+(\\.[a-zA-Z0-9\\-+_=]+)*\\.[a-zA-Z]{2,6})>?($|\\b))", Pattern.MULTILINE
            ), ""),
            new ImmutablePair<>(Pattern.compile("<>", Pattern.MULTILINE), ""),
            new ImmutablePair<>(Pattern.compile("((^)[:<>#|\\- ]+)", Pattern.MULTILINE), ""),
            new ImmutablePair<>(Pattern.compile(
                    "(^|\\b)_([a-zA-Z0-9'\\-_]+(\\s+[a-zA-Z0-9'\\-_]+){0,4})_($|\\b)", Pattern.MULTILINE
            ), "$2"),
            new ImmutablePair<>(Pattern.compile("\"+|``|''", Pattern.MULTILINE), ""),
            new ImmutablePair<>(Pattern.compile("\\*+", Pattern.MULTILINE), "")
    );

    private static final List<Pattern> GOOD_TOKEN_PATTERNS = Stream.of(
            REGEXP_ENG_WORD, REGEXP_ENG_WORD2, REGEXP_ENG_COMMON_WORDS, REGEXP_SLASHED_ENG_WORDS,
            REGEXP_ENG_ABBR, REGEXP_ENG_DASHED_ABBR, REGEXP_ENG_DASHED_ABBR2, REGEXP_ENG_ABBR_WITH_NUMBER
    ).map(regex -> Pattern.compile(
            "[(\\[]?" + regex + "([.!?]|\\.\\.\\.)?[)\\]]?([.,:;!?\\-]|\\.\\.\\.)?", Pattern.CASE_INSENSITIVE
    )).collect(Collectors.toList());

    private static final Pattern PUNCTUATION_MARK_PATTERN = Pattern.compile("[.,:;!?\\-]|\\.\\.\\.");

    private TextUtil() {
        throw new UnsupportedOperationException();
    }

    @SuppressWarnings("AssignmentToMethodParameter")
    @Nonnull
    public static String prettify(@Nonnull String text, boolean onlyWhitelistedTokens) {
        for (Pair<Pattern, String> patternAndReplacement : DOCUMENT_PRETTIFY_PATTERN_AND_REPLACEMENT_PAIRS) {
            text = patternAndReplacement.getLeft().matcher(text).replaceAll(patternAndReplacement.getRight());
        }

        if (onlyWhitelistedTokens) {
            StringBuilder pureTextBuilder = new StringBuilder();
            StringTokenizer textTokenizer = new StringTokenizer(text, WHITESPACE_CHARS, false);

            while (textTokenizer.hasMoreTokens()) {
                String token = textTokenizer.nextToken();

                if (StringUtil.isBlank(token)) {
                    continue;
                }

                if (GOOD_TOKEN_PATTERNS.stream().anyMatch(pattern -> pattern.matcher(token).matches())) {
                    if (pureTextBuilder.length() > 0) {
                        pureTextBuilder.append(' ');
                    }

                    pureTextBuilder.append(token);
                } else if (PUNCTUATION_MARK_PATTERN.matcher(token).matches()) {
                    pureTextBuilder.append(token);
                } else if (PUNCTUATION_MARK_PATTERN.matcher(token).find(token.length() - 1) && (
                        token.length() == 1 || !PUNCTUATION_MARK_PATTERN.matcher(
                                token.substring(0, token.length() - 1)
                        ).find(token.length() - 2)
                )) {
                    pureTextBuilder.append(token.charAt(token.length() - 1));
                }
            }

            text = pureTextBuilder.toString();
        }

        return text;
    }
}
