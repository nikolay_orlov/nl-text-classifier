package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.lineate.internal.nltc.common.math.matrix.DoubleMatrix;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.math3.linear.OpenMapRealMatrix;
import org.apache.commons.math3.linear.SparseRealMatrix;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class ApacheSparseDoubleMatrix implements DoubleMatrix {
    private final SparseRealMatrix matrix;

    public ApacheSparseDoubleMatrix(int rowCount, int colCount) {
        this.matrix = new OpenMapRealMatrix(rowCount, colCount);
    }

    @Override
    public int getRowCount() {
        return matrix.getRowDimension();
    }

    @Override
    public int getColCount() {
        return matrix.getColumnDimension();
    }

    @Override
    public void set(int row, int col, double value) {
        matrix.setEntry(row, col, value);
    }

    @Override
    public double get(int row, int col) {
        return matrix.getEntry(row, col);
    }

    @Override
    public void clear() {
        throw new NotImplementedException("Unnecessary method for experimental matrix implementation.");
    }
}
