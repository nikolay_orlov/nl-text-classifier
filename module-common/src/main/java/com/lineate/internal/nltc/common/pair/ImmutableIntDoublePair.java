package com.lineate.internal.nltc.common.pair;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Comparator;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@SuppressWarnings("WeakerAccess")
@Immutable
public final class ImmutableIntDoublePair implements Comparable<ImmutableIntDoublePair> {
    private static final Comparator<ImmutableIntDoublePair> COMPARATOR = Comparator.comparingInt(
            ImmutableIntDoublePair::getLeft
    ).thenComparingDouble(
            ImmutableIntDoublePair::getRight
    );

    private final int left;
    private final double right;

    public ImmutableIntDoublePair(int left, double right) {
        this.left = left;
        this.right = right;
    }

    @Contract(pure = true)
    public int getLeft() {
        return left;
    }

    @Contract(pure = true)
    public double getRight() {
        return right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImmutableIntDoublePair pair = (ImmutableIntDoublePair) o;

        return left == pair.left && Double.compare(pair.right, right) == 0;
    }

    @Override
    public int hashCode() {
        int result = left;
        result = result * 32323 + Double.hashCode(right);
        return result;
    }

    @Override
    public String toString() {
        return String.format("ImmutableIntDoublePair {left=%d, right=%s}", left, right);
    }

    @Override
    public int compareTo(@NotNull ImmutableIntDoublePair pair) {
        return COMPARATOR.compare(this, pair);
    }
}
