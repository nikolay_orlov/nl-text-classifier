package com.lineate.internal.nltc.common.math.matrix;

import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;

import javax.annotation.Nonnull;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class HppcRowBasedSparseDoubleMatrix extends HppcBaseSparseDoubleMatrix implements RowBasedSparseDoubleMatrix {
    public HppcRowBasedSparseDoubleMatrix(int rowCount, int colCount) {
        super(rowCount, colCount);
    }

    @Override
    public int getRowCount() {
        return getPrimaryKeyLength();
    }

    @Override
    public int getColCount() {
        return getSecondaryKeyLength();
    }

    @Override
    public void set(int row, int col, double value) {
        super.set(row, col, value);
    }

    @Override
    public double get(int row, int col) {
        return super.get(row, col);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseRow(int row) {
        return getSparseVector(row);
    }
}
