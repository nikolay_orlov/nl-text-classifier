package com.lineate.internal.nltc.common.math.vector;

import com.carrotsearch.hppc.IntDoubleHashMap;
import com.carrotsearch.hppc.IntDoubleMap;
import com.carrotsearch.hppc.procedures.IntDoubleProcedure;
import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.pair.ImmutableIntDoublePair;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class HppcSparseDoubleVector implements SparseDoubleVector {
    private final IntDoubleMap valueByIndex;
    private final int length;

    public HppcSparseDoubleVector() {
        this(Integer.MAX_VALUE);
    }

    public HppcSparseDoubleVector(int length) {
        Preconditions.checkArgument(length > 0);

        this.valueByIndex = new IntDoubleHashMap();
        this.length = length;
    }

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public void set(int index, double value) {
        Preconditions.checkArgument(index >= 0 && index < length);

        if (Double.compare(value, 0.0D) == 0) {
            valueByIndex.remove(index);
        } else {
            valueByIndex.put(index, value);
        }
    }

    @Override
    public double get(int index) {
        Preconditions.checkArgument(index >= 0 && index < length);

        return valueByIndex.getOrDefault(index, 0.0D);
    }

    @Override
    public void clear() {
        valueByIndex.clear();
    }

    @Nonnull
    @Override
    public List<ImmutableIntDoublePair> getNonZeros() {
        List<ImmutableIntDoublePair> indexAndValuePairs = new ArrayList<>(valueByIndex.size());

        valueByIndex.forEach(
                (IntDoubleProcedure) (index, value) -> indexAndValuePairs.add(new ImmutableIntDoublePair(index, value))
        );

        indexAndValuePairs.sort(Comparator.comparingInt(ImmutableIntDoublePair::getLeft));
        return Collections.unmodifiableList(indexAndValuePairs);
    }
}
