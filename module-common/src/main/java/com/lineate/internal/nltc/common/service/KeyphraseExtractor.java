package com.lineate.internal.nltc.common.service;

import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.model.Keyphrase;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface KeyphraseExtractor {
    @Nonnull
    List<Keyphrase> extract(@Nonnull Document document) throws IOException;
}
