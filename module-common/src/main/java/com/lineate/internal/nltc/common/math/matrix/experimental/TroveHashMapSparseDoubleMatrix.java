package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.math.matrix.DoubleMatrix;
import gnu.trove.impl.Constants;
import gnu.trove.map.TIntDoubleMap;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntDoubleHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class TroveHashMapSparseDoubleMatrix implements DoubleMatrix {
    private final TIntObjectMap<TIntDoubleMap> valueByColByRow = new TIntObjectHashMap<>(
            Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, 0
    );

    private final int rowCount;
    private final int colCount;

    public TroveHashMapSparseDoubleMatrix(int rowCount, int colCount) {
        Preconditions.checkArgument(rowCount > 0);
        Preconditions.checkArgument(colCount > 0);

        this.rowCount = rowCount;
        this.colCount = colCount;
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColCount() {
        return colCount;
    }

    @Override
    public void set(int row, int col, double value) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        if (Double.compare(value, 0.0D) == 0) {
            TIntDoubleMap valueByCol = valueByColByRow.get(row);

            if (valueByCol != null) {
                valueByCol.remove(col);

                if (valueByCol.isEmpty()) {
                    valueByColByRow.remove(row);
                }
            }
        } else {
            TIntDoubleMap valueByCol = valueByColByRow.get(row);

            if (valueByCol == null) {
                valueByCol = new TIntDoubleHashMap(Constants.DEFAULT_CAPACITY, Constants.DEFAULT_LOAD_FACTOR, 0, 0.0D);
                valueByColByRow.put(row, valueByCol);
            }

            valueByCol.put(col, value);
        }
    }

    @Override
    public double get(int row, int col) {
        Preconditions.checkArgument(row >= 0 && row < rowCount);
        Preconditions.checkArgument(col >= 0 && col < colCount);

        TIntDoubleMap valueByCol = valueByColByRow.get(row);

        return valueByCol == null ? 0.0D : valueByCol.get(col);
    }

    @Override
    public void clear() {
        valueByColByRow.clear();
    }
}
