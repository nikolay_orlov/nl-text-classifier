package com.lineate.internal.nltc.common.math.matrix.experimental;

import com.lineate.internal.nltc.common.math.matrix.SparseDoubleMatrix;
import com.lineate.internal.nltc.common.math.vector.HppcSparseDoubleVector;
import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;
import org.apache.commons.lang3.NotImplementedException;
import org.la4j.matrix.SparseMatrix;
import org.la4j.matrix.sparse.CRSMatrix;

import javax.annotation.Nonnull;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class La4jSparseDoubleMatrix implements SparseDoubleMatrix {
    private final SparseMatrix matrix;

    public La4jSparseDoubleMatrix(int rowCount, int colCount) {
        matrix = new CRSMatrix(rowCount, colCount);
    }

    @Override
    public int getRowCount() {
        return matrix.rows();
    }

    @Override
    public int getColCount() {
        return matrix.columns();
    }

    @Override
    public void set(int row, int col, double value) {
        matrix.set(row, col, value);
    }

    @Override
    public double get(int row, int col) {
        return matrix.get(row, col);
    }

    @Override
    public void clear() {
        throw new NotImplementedException("Unnecessary method for experimental matrix implementation.");
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseCol(int col) {
        SparseDoubleVector sparseCol = new HppcSparseDoubleVector(getRowCount());
        matrix.eachNonZeroInColumn(col, sparseCol::set);
        return sparseCol;
    }

    @Nonnull
    @Override
    public SparseDoubleVector getSparseRow(int row) {
        SparseDoubleVector sparseRow = new HppcSparseDoubleVector(getColCount());
        matrix.eachNonZeroInRow(row, sparseRow::set);
        return sparseRow;
    }
}
