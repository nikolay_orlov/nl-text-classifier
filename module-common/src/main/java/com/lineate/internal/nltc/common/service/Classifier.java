package com.lineate.internal.nltc.common.service;

import com.lineate.internal.nltc.common.model.Category;
import com.lineate.internal.nltc.common.model.Keyphrase;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.util.Collection;
import java.util.List;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface Classifier {
	public static final double DEFAULT_PROBABILITY_THRESHOLD = 0.1;
    @Nullable
    String classify(@Nonnull List<Keyphrase> keyphrases);
    
    List<Category> classify(@Nonnull List<Keyphrase> keyphrases, double probability);
}