package com.lineate.internal.nltc.common.math.matrix;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface SparseDoubleMatrix extends RowBasedSparseDoubleMatrix, ColBasedSparseDoubleMatrix {
}
