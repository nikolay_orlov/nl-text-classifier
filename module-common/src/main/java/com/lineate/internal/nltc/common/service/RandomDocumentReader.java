package com.lineate.internal.nltc.common.service;

import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.model.Document;

import javax.annotation.*;
import java.io.IOException;
import java.util.Objects;
import java.util.Random;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class RandomDocumentReader implements DocumentReader {
    @SuppressWarnings("UnsecureRandomNumberGeneration")
    private final Random random = new Random();

    @Nonnull
    private final DocumentReader reader;

    @Nonnegative
    private final double documentReadProbability;

    public RandomDocumentReader(@Nonnull DocumentReader reader, @Nonnegative double documentReadProbability) {
        Preconditions.checkArgument(documentReadProbability >= 0.0D && documentReadProbability <= 1.0D);

        this.reader = Objects.requireNonNull(reader);
        this.documentReadProbability = documentReadProbability;
    }

    @Nullable
    @Override
    public Document read() throws IOException {
        Document document = reader.read();

        while (document != null && random.nextDouble() >= documentReadProbability) {
            document = reader.read();
        }

        return document;
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}
