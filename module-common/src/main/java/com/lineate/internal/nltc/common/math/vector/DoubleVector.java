package com.lineate.internal.nltc.common.math.vector;

import javax.annotation.Nonnegative;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface DoubleVector {
    int getLength();

    void set(@Nonnegative int index, double value);
    double get(@Nonnegative int index);

    void clear();
}
