package com.lineate.internal.nltc.common.math.matrix;

import com.lineate.internal.nltc.common.math.vector.SparseDoubleVector;

import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface ColBasedSparseDoubleMatrix extends DoubleMatrix {
    @Nonnull
    SparseDoubleVector getSparseCol(@Nonnegative int col);
}
