package com.lineate.internal.nltc.cli;

import com.google.common.base.Preconditions;
import com.lineate.internal.nltc.common.math.matrix.*;
import com.lineate.internal.nltc.common.math.matrix.experimental.JavaMapCombinedSparseDoubleMatrix;
import ru.sladethe.common.text.FormatUtil;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class Benchmark {
    private static final int ROW_COUNT = 100_000;
    private static final int COL_COUNT = 1_000_000;
    private static final int NON_EMPTY_VALUE_COUNT_PER_ROW = 100;
    private static final long RANDOM_SEED = -1321435498735L;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 3; ++i) {
            // benchmark(new HashMapSparseDoubleMatrix(ROW_COUNT, COL_COUNT, 0.0D));
            benchmark(new JavaMapCombinedSparseDoubleMatrix(
                    ROW_COUNT, COL_COUNT, 0.0D, new HashMap<>(), new HashMap<>(), HashMap::new
            ));
            benchmark(new HppcRowBasedSparseDoubleMatrix(ROW_COUNT, COL_COUNT));
            benchmark(new HppcColBasedSparseDoubleMatrix(ROW_COUNT, COL_COUNT));
            benchmark(new HppcCombinedSparseDoubleMatrix(ROW_COUNT, COL_COUNT));
            // benchmark(new HashMapLongKeySparseDoubleMatrix(ROW_COUNT, COL_COUNT, 0.0D));
            // benchmark(new TroveHashMapSparseDoubleMatrix(ROW_COUNT, COL_COUNT));
            // benchmark(new TroveHashMapLongKeySparseDoubleMatrix(ROW_COUNT, COL_COUNT));
            // benchmark(new ApacheSparseDoubleMatrix(ROW_COUNT, COL_COUNT));
            // benchmark(new La4jSparseDoubleMatrix(ROW_COUNT, COL_COUNT));
            // benchmark(new La4jCombinedSparseDoubleMatrix(ROW_COUNT, COL_COUNT));
            // benchmark(new TrieMapSparseDoubleMatrix(ROW_COUNT, COL_COUNT, 0.0D));
            /*benchmark(new DoubleMatrixHolder(new JavaMapCombinedSparseDoubleMatrix(
                    ROW_COUNT, COL_COUNT, 0.0D, new HashMap<>(), new HashMap<>(), HashMap::new
            )) {
                @Override
                protected DoubleMatrix optimize(DoubleMatrix matrix) {
                    return new La4jCombinedSparseDoubleMatrix((SparseDoubleMatrix) matrix);
                }
            });*/
            System.out.println();
        }

        System.out.flush();
        Thread.sleep(1L);
    }

    private static void benchmark(@Nonnull DoubleMatrix matrix) throws InterruptedException {
        benchmark(new DoubleMatrixHolder(matrix) {
            @Override
            protected DoubleMatrix optimize(DoubleMatrix _matrix) {
                return _matrix;
            }
        });
    }

    private static void benchmark(@Nonnull DoubleMatrixHolder matrixHolder) throws InterruptedException {
        benchmarkPut(matrixHolder);
        benchmarkGetNonZeroByRows(matrixHolder.getMatrix());
        benchmarkGetNonZeroByCols(matrixHolder.getMatrix());
        // benchmarkGet(matrixHolder.getMatrix());
        System.out.println();
    }

    @SuppressWarnings("CallToSystemGC")
    private static void benchmarkPut(@Nonnull DoubleMatrixHolder matrixHolder) throws InterruptedException {
        @SuppressWarnings("UnsecureRandomNumberGeneration") Random random = new Random(RANDOM_SEED);

        System.gc();
        Thread.sleep(100L);
        System.gc();

        long startTimeNanos = System.nanoTime();

        for (int row = 0; row < ROW_COUNT; ++row) {
            for (int i = 0; i < NON_EMPTY_VALUE_COUNT_PER_ROW; ++i) {
                DoubleMatrix matrix = matrixHolder.getMatrix();
                int col = random.nextInt(matrix.getColCount());
                matrix.set(row, col, 1.0D);
            }
        }

        matrixHolder.optimize();

        long finishTimeNanos = System.nanoTime();

        System.gc();
        Thread.sleep(100L);
        System.gc();

        System.out.printf(
                "Tested PUT of %s in %d ms, used %s.", matrixHolder.getMatrix().getClass().getSimpleName(),
                TimeUnit.NANOSECONDS.toMillis(finishTimeNanos - startTimeNanos),
                FormatUtil.formatDataSize(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())
        );
    }

    private static void benchmarkGetNonZeroByRows(@Nonnull DoubleMatrix matrix) {
        if (matrix instanceof RowBasedSparseDoubleMatrix) {
            RowBasedSparseDoubleMatrix rowBasedSparseDoubleMatrix = (RowBasedSparseDoubleMatrix) matrix;

            long startTimeNanos = System.nanoTime();

            for (int row = 0; row < ROW_COUNT; ++row) {
                rowBasedSparseDoubleMatrix.getSparseRow(row);
            }

            long finishTimeNanos = System.nanoTime();

            System.out.printf(
                    " Tested GET != 0 by rows in %d ms.",
                    TimeUnit.NANOSECONDS.toMillis(finishTimeNanos - startTimeNanos)
            );
        }
    }

    private static void benchmarkGetNonZeroByCols(@Nonnull DoubleMatrix matrix) {
        if (matrix instanceof ColBasedSparseDoubleMatrix) {
            ColBasedSparseDoubleMatrix colBasedSparseDoubleMatrix = (ColBasedSparseDoubleMatrix) matrix;

            long startTimeNanos = System.nanoTime();

            for (int col = 0; col < COL_COUNT; ++col) {
                colBasedSparseDoubleMatrix.getSparseCol(col);
            }

            long finishTimeNanos = System.nanoTime();

            System.out.printf(
                    " Tested GET != 0 by cols in %d ms.",
                    TimeUnit.NANOSECONDS.toMillis(finishTimeNanos - startTimeNanos)
            );
        }
    }

    private static void benchmarkGet(@Nonnull DoubleMatrix matrix) {
        @SuppressWarnings("UnsecureRandomNumberGeneration") Random random = new Random(RANDOM_SEED);

        long startTimeNanos = System.nanoTime();

        for (int row = 0; row < ROW_COUNT; ++row) {
            Set<Integer> presentCols = new HashSet<>();

            for (int i = 0; i < NON_EMPTY_VALUE_COUNT_PER_ROW; ++i) {
                presentCols.add(random.nextInt(matrix.getColCount()));
            }

            for (int col = 0; col < COL_COUNT; ++col) {
                Preconditions.checkArgument(Double.compare(
                        matrix.get(row, col),
                        presentCols.contains(col) ? 1.0D : 0.0D
                ) == 0);
            }
        }

        long finishTimeNanos = System.nanoTime();

        System.out.printf(" Tested GET in %d ms.", TimeUnit.NANOSECONDS.toMillis(finishTimeNanos - startTimeNanos));
    }

    private abstract static class DoubleMatrixHolder {
        private DoubleMatrix matrix;

        private DoubleMatrixHolder(DoubleMatrix matrix) {
            this.matrix = matrix;
        }

        public DoubleMatrix getMatrix() {
            return matrix;
        }

        public void optimize() {
            matrix = optimize(matrix);
        }

        protected abstract DoubleMatrix optimize(DoubleMatrix matrix);
    }
}
