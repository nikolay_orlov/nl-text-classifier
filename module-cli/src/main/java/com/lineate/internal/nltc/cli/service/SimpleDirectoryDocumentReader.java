package com.lineate.internal.nltc.cli.service;

import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.service.DocumentReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@SuppressWarnings("WeakerAccess")
public class SimpleDirectoryDocumentReader implements DocumentReader {
    @Nonnull
    private final File directory;

    private final boolean recursive;

    @Nullable
    private final Charset charset;

    @Nullable
    private final String documentCategory;

    @Nonnull
    private final Predicate<File> fileMatcher;

    private File[] files;

    private int fileIndex = -1;

    public SimpleDirectoryDocumentReader(
            @Nonnull File directory, boolean recursive, @Nullable Charset charset, @Nullable String documentCategory,
            @Nonnull Predicate<File> fileMatcher
    ) {
        this.directory = Objects.requireNonNull(directory);
        this.recursive = recursive;
        this.charset = charset;
        this.documentCategory = StringUtils.trimToNull(documentCategory);
        this.fileMatcher = Objects.requireNonNull(fileMatcher);
    }

    public SimpleDirectoryDocumentReader(
            @Nonnull File directory, boolean recursive, @Nullable Charset charset, @Nullable String documentCategory
    ) {
        this(directory, recursive, charset, documentCategory, file -> true);
    }

    @Nullable
    @Override
    public Document read() throws IOException {
        if (files == null) {
            if (!directory.isDirectory()) {
                throw new FileNotFoundException("Documents directory does not exist: '" + directory.getPath() + "'.");
            }

            files = FileUtils.listFiles(directory, null, recursive).stream().filter(fileMatcher).toArray(File[]::new);
            fileIndex = 0;
        }

        if (fileIndex >= files.length) {
            return null;
        }

        File file = files[fileIndex++];

        String text = StringUtils.trimToNull(FileUtils.readFileToString(
                file, ObjectUtils.defaultIfNull(charset, StandardCharsets.UTF_8)
        ));

        return text == null ? read() : new Document(
                text, ObjectUtils.defaultIfNull(documentCategory, directory.getName())
        );
    }

    @Override
    public void close() {
        files = null;
        fileIndex = -1;
    }
}
