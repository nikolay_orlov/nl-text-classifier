package com.lineate.internal.nltc.cli;

import com.lineate.internal.nltc.cli.command.ClassifyCommand;
import com.lineate.internal.nltc.cli.command.LearnCommand;
import com.lineate.internal.nltc.cli.config.ApplicationProperties;
import com.lineate.internal.nltc.cli.service.*;
import com.lineate.internal.nltc.common.service.*;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import ru.sladethe.common.io.FileUtil;
import ru.sladethe.common.lang.ObjectUtil;
import ru.sladethe.common.math.NumberUtil;
import ru.sladethe.common.text.StringUtil;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class Runner {
    private static final String DEFAULT_DATA_FILE_NAME = "nltc.db";

    private static final String DATA_PATH_OPTION = "data-path";
    private static final String DOCUMENT_CATEGORY_OPTION = "document-category";
    private static final String DOCUMENT_PREPROCESSOR_OPTION = "document-preprocessor";
    private static final String DOCUMENT_READER_OPTION = "document-reader";
    private static final String RESET_DATA_OPTION = "reset-data";
    private static final String SOURCE_PATH_OPTION = "source-path";
    private static final String TAKE_RANDOM_OPTION = "take-random";

    public static void main(String[] args) throws IOException {
        Options options = getBaseOptions();

        if (args.length < 1) {
            printHelp(options, 1);
        }

        String command = StringUtils.trimToEmpty(args[0]);
        addCommandOptions(options, command);

        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine;

        try {
            commandLine = parser.parse(options, args, false);
        } catch (ParseException e) {
            printHelp(options, e, 1);
            return;
        }

        File dataFile = new File(commandLine.getOptionValue(DATA_PATH_OPTION, DEFAULT_DATA_FILE_NAME));

        byte[] data = readData(commandLine, dataFile);

        switch (command) {
            case "learn":
                executeLearnCommand(command, commandLine, options, data, dataFile);
                break;
            case "classify":
                executeClassifyCommand(command, commandLine, options, data);
                break;
            default:
                throw new IllegalStateException("Unexpected command: '" + command + "'.");
        }
    }

    private static void executeLearnCommand(
            String command, CommandLine commandLine, Options options, @Nullable byte[] data, File dataFile
    ) throws IOException {
        DocumentReader documentReader = getDocumentReader(command, commandLine, options);
        KeyphraseExtractor keyphraseExtractor = getKeyphraseExtractor(commandLine);

        ClassifierImpl classifier = new ClassifierImpl();
        ObjectUtil.ifNotNull(data, classifier::load);

        new LearnCommand(documentReader, keyphraseExtractor, classifier).execute();

        saveData(dataFile, classifier.save());
    }

    private static void executeClassifyCommand(
            String command, CommandLine commandLine, Options options, @Nullable byte[] data
    ) throws IOException {
        DocumentReader documentReader = getDocumentReader(command, commandLine, options);
        KeyphraseExtractor keyphraseExtractor = getKeyphraseExtractor(commandLine);

        ClassifierImpl classifier = new ClassifierImpl();
        ObjectUtil.ifNotNull(data, classifier::load);

        new ClassifyCommand(documentReader, keyphraseExtractor, classifier).execute().forEach(System.out::println);
    }

    @Nonnull
    private static Options getBaseOptions() {
        Options options = new Options();

        options.addOption(
                null, DATA_PATH_OPTION, true, "override data file path, default is 'nltc.db'"
        );

        options.addOption(
                null, DOCUMENT_PREPROCESSOR_OPTION, true,
                "specify document preprocessor, e.g. 'prettify'"
        );

        options.addOption(
                null, TAKE_RANDOM_OPTION, true,
                "1%-99%, take only the specified randomly selected amount of provided documents and skip others"
        );

        options.addOption(
                null, ApplicationProperties.KPE_API_URL_PROPERTY, true,
                "override keyphrase extractor URL in application properties"
        );

        return options;
    }

    private static void addCommandOptions(@Nonnull Options options, @Nonnull String command) {
        options.addRequiredOption(
                null, DOCUMENT_READER_OPTION, true,
                "specify document reader, e.g. 'root-directory', 'simple-directory', 'plain-document'"
        );

        options.addRequiredOption(null, SOURCE_PATH_OPTION, true, "path to the source of document(s)");

        switch (command) {
            case "learn":
                options.addOption(
                        null, DOCUMENT_CATEGORY_OPTION, true,
                        "override category of document(s); some document readers may ignore this option"
                );

                options.addOption(null, RESET_DATA_OPTION, false, "forget and delete existing data");

                break;
            case "classify":
                break;
            default:
                printHelp(options, 1);
        }
    }

    @SuppressWarnings("CallToSystemExit")
    private static void printHelp(@Nonnull Options options, @Nullable String errorMessage, @Nullable Integer exitCode) {
        ObjectUtil.ifNotNull(errorMessage, System.out::println);
        new HelpFormatter().printHelp("nltc learn|classify [option]...\n", options);
        ObjectUtil.ifNotNull(exitCode, System::exit);
    }

    private static void printHelp(@Nonnull Options options, @Nullable Exception e, @Nullable Integer exitCode) {
        printHelp(options, ObjectUtil.mapNotNull(e, Throwable::getMessage), exitCode);
    }

    private static void printHelp(@Nonnull Options options, @Nullable Integer exitCode) {
        printHelp(options, (String) null, exitCode);
    }

    @Nullable
    private static byte[] readData(@Nonnull CommandLine commandLine, File dataFile) {
        if (commandLine.hasOption(RESET_DATA_OPTION)) {
            FileUtils.deleteQuietly(dataFile);
            return null;
        } else {
            try {
                return FileUtil.getCriticalFileBytes(dataFile);
            } catch (IOException e) {
                System.err.println("Can't read saved data: " + e.getMessage());
                return null;
            }
        }
    }

    private static void saveData(File dataFile, byte[] data) {
        try {
            FileUtil.writeCriticalFile(dataFile, data);
        } catch (IOException e) {
            System.err.println("Can't save data: " + e.getMessage());
        }
    }

    @Nonnull
    private static DocumentReader getDocumentReader(
            @Nonnull String command, @Nonnull CommandLine commandLine, @Nonnull Options options
    ) {
        DocumentReader documentReader = getBaseDocumentReader(command, commandLine, options);

        if (commandLine.hasOption(TAKE_RANDOM_OPTION)) {
            String documentReadProbabilityPercents = commandLine.getOptionValue(TAKE_RANDOM_OPTION);

            Matcher documentReadProbabilityMatcher = Pattern.compile("([1-9][0-9]?)%")
                    .matcher(documentReadProbabilityPercents);

            if (!documentReadProbabilityMatcher.matches()) {
                printHelp(options, String.format("Illegal argument for '%s' option.", TAKE_RANDOM_OPTION), 1);
            }

            documentReader = new RandomDocumentReader(
                    documentReader, NumberUtil.toInt(documentReadProbabilityMatcher.group(1)) / 100.0D
            );
        }

        if (commandLine.hasOption(DOCUMENT_PREPROCESSOR_OPTION)) {
            String documentPreprocessor = commandLine.getOptionValue(DOCUMENT_PREPROCESSOR_OPTION);

            if ("prettify".equals(documentPreprocessor)) {
                documentReader = new PrettifyDocumentReader(documentReader);
            } else {
                String message = String.format(
                        "Unsupported document preprocessor: '%s'.",
                        commandLine.getOptionValue(DOCUMENT_PREPROCESSOR_OPTION)
                );

                printHelp(options, message, 1);
                throw new IllegalArgumentException(message);
            }
        }

        return documentReader;
    }

    @Nonnull
    private static DocumentReader getBaseDocumentReader(
            @Nonnull String command, @Nonnull CommandLine commandLine, @Nonnull Options options
    ) {
        File sourcePath = new File(commandLine.getOptionValue(SOURCE_PATH_OPTION));
        String category = StringUtils.trimToNull(commandLine.getOptionValue(DOCUMENT_CATEGORY_OPTION, null));

        switch (commandLine.getOptionValue(DOCUMENT_READER_OPTION)) {
            case "root-directory":
                return new RootDirectoryDocumentReader(sourcePath, UTF_8);
            case "simple-directory":
                return new SimpleDirectoryDocumentReader(sourcePath, true, UTF_8, category);
            case "plain-document":
                if (category == null && !"classify".equals(command)) {
                    printHelp(options, "Missing required option for plain document reader: document-category", 1);
                }
                return new PlainFileDocumentReader(sourcePath, UTF_8, category);
            default:
                String message = String.format(
                        "Unsupported document reader: '%s'.", commandLine.getOptionValue(DOCUMENT_READER_OPTION)
                );

                printHelp(options, message, 1);
                throw new IllegalArgumentException(message);
        }
    }

    @Nonnull
    private static KeyphraseExtractor getKeyphraseExtractor(@Nonnull CommandLine commandLine) {
        String kpeApiUrl = commandLine.getOptionValue(ApplicationProperties.KPE_API_URL_PROPERTY);

        return new RemoteKeyphraseExtractor(
                StringUtil.isBlank(kpeApiUrl) ? ApplicationProperties.getInstance().getKpeUrl() : kpeApiUrl
        );
    }
}
