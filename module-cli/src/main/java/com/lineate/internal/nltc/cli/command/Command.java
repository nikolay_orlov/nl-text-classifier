package com.lineate.internal.nltc.cli.command;

import java.io.IOException;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public interface Command<T> {
    T execute() throws IOException;
}
