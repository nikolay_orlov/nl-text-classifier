package com.lineate.internal.nltc.cli.command;

import static com.lineate.internal.nltc.common.service.Classifier.DEFAULT_PROBABILITY_THRESHOLD;
import com.lineate.internal.nltc.common.model.Category;
import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.model.Keyphrase;
import com.lineate.internal.nltc.common.service.*;

import javax.annotation.Nonnull;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class ClassifyCommand implements Command<List<String>> {
    @Nonnull
    private final DocumentReader documentReader;

    @Nonnull
    private final KeyphraseExtractor keyphraseExtractor;

    @Nonnull
    private final Classifier classifier;

    public ClassifyCommand(
            @Nonnull DocumentReader documentReader, @Nonnull KeyphraseExtractor keyphraseExtractor,
            @Nonnull Classifier classifier
    ) {
        this.documentReader = Objects.requireNonNull(documentReader);
        this.keyphraseExtractor = Objects.requireNonNull(keyphraseExtractor);
        this.classifier = Objects.requireNonNull(classifier);
    }

    @Override
    public List<String> execute() throws IOException {
        List<String> categories = new ArrayList<>();

        Document document;

        while ((document = documentReader.read()) != null) {
            List<Keyphrase> keyphrases = keyphraseExtractor.extract(document);
            categories.addAll(classifier.classify(keyphrases, DEFAULT_PROBABILITY_THRESHOLD).stream().map(c -> String.format("%s : %f", c.getClassId(), c.getProbability())).collect(Collectors.toList()));
        }

        return Collections.unmodifiableList(categories);
    }
}
