package com.lineate.internal.nltc.cli.command;

import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.model.Keyphrase;
import com.lineate.internal.nltc.common.service.*;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class LearnCommand implements Command<Classifier> {
    @Nonnull
    private final DocumentReader documentReader;

    @Nonnull
    private final KeyphraseExtractor keyphraseExtractor;

    @Nonnull
    private final ClassifierImpl classifier;

    public LearnCommand(
            @Nonnull DocumentReader documentReader, @Nonnull KeyphraseExtractor keyphraseExtractor,
            @Nonnull ClassifierImpl classifier
    ) {
        this.documentReader = Objects.requireNonNull(documentReader);
        this.keyphraseExtractor = Objects.requireNonNull(keyphraseExtractor);
        this.classifier = Objects.requireNonNull(classifier);
    }

    @Override
    public Classifier execute() throws IOException {
        Document document;

        while ((document = documentReader.read()) != null) {
            if (!classifier.isRegistered(document)) {
                List<Keyphrase> keyphrases = keyphraseExtractor.extract(document);
                classifier.register(document, keyphrases);
            }
        }

        classifier.calculateAndGetClassificationMatrix();
        return classifier;
    }
}
