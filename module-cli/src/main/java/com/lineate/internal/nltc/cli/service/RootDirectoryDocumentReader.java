package com.lineate.internal.nltc.cli.service;

import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.service.DocumentReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
@SuppressWarnings("WeakerAccess")
public class RootDirectoryDocumentReader implements DocumentReader {
    @Nonnull
    private final File rootDirectory;

    @Nullable
    private final Charset charset;

    @Nonnull
    private final Predicate<File> fileMatcher;

    private File[] directories;

    private int directoryIndex = -1;

    private DocumentReader reader;

    public RootDirectoryDocumentReader(
            @Nonnull File rootDirectory, @Nullable Charset charset, @Nonnull Predicate<File> fileMatcher
    ) {
        this.rootDirectory = Objects.requireNonNull(rootDirectory);
        this.charset = charset;
        this.fileMatcher = Objects.requireNonNull(fileMatcher);
    }

    public RootDirectoryDocumentReader(@Nonnull File rootDirectory, @Nullable Charset charset) {
        this(rootDirectory, charset, file -> true);
    }

    @Nullable
    @Override
    public Document read() throws IOException {
        if (directories == null) {
            if (!rootDirectory.isDirectory()) {
                throw new FileNotFoundException("Root directory does not exist: '" + rootDirectory.getPath() + "'.");
            }

            directories = ObjectUtils.defaultIfNull(
                    rootDirectory.listFiles(File::isDirectory), FileUtils.EMPTY_FILE_ARRAY
            );

            directoryIndex = 0;
            reader = null;
        }

        if (reader != null) {
            return internalRead();
        }

        if (directoryIndex >= directories.length) {
            return null;
        }

        File directory = directories[directoryIndex++];

        reader = new SimpleDirectoryDocumentReader(directory, true, charset, directory.getName(), fileMatcher);

        return internalRead();
    }

    @Nullable
    private Document internalRead() throws IOException {
        Document document = reader.read();

        if (document == null) {
            reader.close();
            reader = null;
            return read();
        } else {
            return document;
        }
    }

    @Override
    public void close() throws IOException {
        directories = null;
        directoryIndex = -1;

        if (reader != null) {
            reader.close();
            reader = null;
        }
    }
}
