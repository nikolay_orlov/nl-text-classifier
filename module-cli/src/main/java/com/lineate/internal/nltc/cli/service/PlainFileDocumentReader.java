package com.lineate.internal.nltc.cli.service;

import com.lineate.internal.nltc.common.model.Document;
import com.lineate.internal.nltc.common.service.DocumentReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class PlainFileDocumentReader implements DocumentReader {
    @Nonnull
    private final File path;

    @Nullable
    private final Charset charset;

    @Nullable
    private final String documentCategory;

    private boolean completed;

    public PlainFileDocumentReader(@Nonnull File path, @Nullable Charset charset, @Nullable String documentCategory) {
        this.path = Objects.requireNonNull(path);
        this.charset = charset;
        this.documentCategory = StringUtils.trimToNull(documentCategory);
    }

    @Nullable
    @Override
    public Document read() throws IOException {
        if (completed) {
            return null;
        }

        completed = true;

        String text = StringUtils.trimToNull(FileUtils.readFileToString(
                path, ObjectUtils.defaultIfNull(charset, StandardCharsets.UTF_8)
        ));

        return text == null ? null : new Document(text, documentCategory);
    }

    @Override
    public void close() {
        completed = false;
    }
}
