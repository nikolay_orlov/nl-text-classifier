package com.lineate.internal.nltc.cli.config;

import com.google.common.base.Preconditions;
import ru.sladethe.common.text.UrlUtil;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Maxim Shipko (mshipko@lineate.com)
 */
public class ApplicationProperties {
    public static final String KPE_API_URL_PROPERTY = "kpe-api.url";

    private static final ApplicationProperties INSTANCE = new ApplicationProperties();

    public static ApplicationProperties getInstance() {
        return INSTANCE;
    }

    private final Properties properties;

    private ApplicationProperties() {
        properties = new Properties();

        try {
            properties.load(ApplicationProperties.class.getResourceAsStream(
                    "/com/lineate/internal/nltc/cli/application.properties"
            ));
        } catch (IOException e) {
            throw new RuntimeException("Can't load application properties.", e);
        }
    }

    public String getKpeUrl() {
        String kpeUrl = properties.getProperty(KPE_API_URL_PROPERTY, "http://localhost:8888/api/keywords");
        Preconditions.checkState(UrlUtil.isValidUrl(kpeUrl));
        return kpeUrl;
    }
}
