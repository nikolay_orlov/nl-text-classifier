# README #

### How do I get set up? ###

* Running KPE service (nikolay_orlov/key-phrase-extractor) is required for this application to work

### Quick usage guide ###

####'learn' command####

~~~~~
java -jar /path/to/jar learn --document-reader root-directory --source-path /path/to/root/directory [--kpe-api.url http://localhost:8888/api/keywords]
~~~~~

Scans root directory for first level child directories. The name of the first level child directory is assumed as the category of all nested document files (even if they are in second or higher level child directories).
The KPE API URL is an optional parameter. By default NL text classifier expects KPE service to listen 8888-th port on local machine.
The operation may take a long time to complete. In case of success the nltc.db file is created.

####'classify' command####

~~~~~
java -jar /path/to/jar classify --document-reader plain-document --source-path /path/to/document [--kpe-api.url http://localhost:8888/api/keywords]
~~~~~

Tries to classify the provided document and prints the category with highest probability to standard output. This operation is fast.

### Contribution ###

* Core development: Maxim Shipko (mshipko@lineate.com, sladethe@gmail.com)
* Code review: Nikolay Orlov